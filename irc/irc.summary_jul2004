/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 *
 * This is a summary of the IRC Evolution Party/Conference which
 * took place on Jul. 29th, 2004 (Thursday) from 1200 to 0000 UTC.
 *
 * The raw data is available in 'irc.party_jul2004'.
 *
 *  Server : irc.freenode.net
 *  Channel: #arabeyes
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

On July 29th, 2004 Arabeyes held its Third annual Evolution Party
(which has turned into a conference of sorts and ought to be called
as such in future years).  What follows is an abridged summary of 
the proceeding with emphasis on what was promised (ie. various TODOs
denoted by '=>') along with various highlights.  The full IRC log
is available for a more verbose view.

  http://cvs.arabeyes.org/viewcvs/ae_admin/irc/irc.party_jul2004

The "party/conference" started in earnest at 12:00pm UTC with
initially 40+ attendees with swelled to approx 70 at some points.


-- Topic: Arabeyes.org and the future
-- By   : Nadim Shaikli, Mohammed Elzubeir, Youcef Rabah Rahal
 - Nadim starts out by thanking everyone for coming and joining.
 - Nadim points out the growth rate that Arabeyes has seen of late and
   notes Arabeyes' work-oriented and results fostering attitude.
 - Nadim gives a special "Thank You" to Arabeyes' dedicated volunteers.
 - Nadim notes the coming of future changes (soon) to bring forth a
   prolonged future for Arabeyes (a more verbose charter, elections,
   guidelines, accepting donations, etc)
 - The docs are all in, http://cvs.arabeyes.org/viewcvs/ae_admin/docs
 - Youcef lists out the large number of newly started projects this past year.
 - Some questions about Public-Relations (PR) were fielded which resulted
   in the need for people to take on the work and responsibility.  Various
   people attest to the need for Arabeyes to be more vocal.
 - Alaa notes the need for Arabeyes to be legalized or "legitimized" since
   various educational/governmental institutions don't talk to anyone unless
   they are "official" in Egypt (and else where in the Arab World).
 =>Arabeyes to look into setting up a non-profit (or NGO or...) legal entity
  
-- Topic: Mandrake
-- By   : Pablo Saratxaga
 - Pablo notes that he's tasked with "making our distribution support as
   many languages as possible"
 - Pablo adds that such support involves writing locale definitions
   (sorting order, etc), packaging fonts, writing keyboard tables,
   finding translators, etc.
 - Pablo notes that Arabic support is rather recent due in part to Arabic's
   complexity as a language, its need for Bidirectionality (Bidi) as well as
   shaping (or joining) in which characters change shape depending on their
   location within a word have complicated things.
 - Pablo notes that thanks to Gtk and Qt toolkits, now most of the
   graphical programs have quite good Arabic support.
 - Pablo notes that there is some cosmetic work left to be done
   (arrow directionality, etc)
 - Pablo notes that coordinated translation work (such as what Arabeyes
   offers its translators) has improved the quality/quantity of work.
 - Pablo notes that Arabic is still missing adequate support in some areas;
   fonts, an Arabic spell checker and working terminals (console and X)
 - A number of people barrage Pablo with thank-yous for his continued
   impressive and respected support and hard-work.
 - Some questions came up regarding testing and how that is done (since some
   bugs reappear from version to version).  Pablo noted that Mandrake doesn't
   currently have the resources to comprehensively test Arabic issue.
 - Pablo notes that testing "cooker" is a good thing.
 - Pablo notes that Mandrake is thinking (not only for Arabic) to make
   language-recipes with things to test for a given language
 =>Arabeyes (Mandrake coordinator) to look into Pablo's mention of
   language-recipes for better Arabic testing
 - A few people point out that OpenOffice.org requires libfreetype with
   the truetype byte interpreter enabled to be installed for it to support
   Arabic and ask if Mandrake could automate its installation along with
   the checking/enabling of the checkbox in openoffice options that enables
   Complex Text Layouts (all to be done automatically).
 - Munzir, an Arabeyes volunteer, notes the creation of a wiki page with all
   the known Arabic Bugs (http://wiki.arabeyes.org/OpenBugs)
 - Fonts are briefly discussed and the khotot project
   (http://www.arabeyes.org/project.php?proj=Khotot) is mentioned upon
   which time Pablo reminded all to be inclusive of other "Arabic"-like
   languages (ie. ones that share the same script).
 - Pablo also suggests that a document talking about "What is required to
   support Arabic in your application" be created to supplement the
   developers' required knowledge for proper Arabic support.
 =>Arabeyes is to look into and create a comprehensive document outlining
   all the basics a developer will require including the GUI look-n-feel
   attributes that are expected, etc.
 =>Pablo to look into a couple of packages that have already been submitted
   (fonts RPMs and an English-Arabic dict format dictionary).

-- Topic: Egypt LUG
-- By   : Alaa Abd-El-Fatah, Mostafa Hussein
 - Alaa starts out by introducing the one month old new Egyptian LUG
   (http://www.eglug.org)
 - Alaa notes that the new LUG's focus is not only on free tech support, but
   also in trying to be proactive in advocating and promoting FOSS in Egypt
 - Alaa notes that the LUG is still going through some growing pains, but is
   slated to be very democratic (all opinions matter).
 - Alaa notes that they've already organized and conducted numerous
   activities in the Sakia Cultural Center along with "DO OR DIE" sessions
   in which they lock various members in a room and tell them to finish a
   task else they don't get to leave.
 - Alaa also notes that the LUG will also start conducting weekly courses
   for GNU/Linux newbies, Alaa also adds that demand seems high.
 - Alaa notes the following Ad gag video (http://www.eglug.org/advert)
 - Mostafa adds that the LUG is also participating in events that address
   the civil society and NGO's in general to spread the word of FOSS in
   that area.
 - Uniball, a LUG member, notes that they forked from the previous Egyptian
   LUG due to the "old lug founder and admins wanting to work in a secret
   way without announcements ... they prefer the cathedral style than the
   bazaar style".
 - Alaa, post a question, notes having 100+ web-registered members of which
   about 40 are active and 20 can be relied upon.
 - A number of people asking questions commend the LUG on its fun attitude
   and note that this LUG's hands-on, get-to-the-public reach-out sessions
   should be a lesson for all to learn from.
 - Mostafa notes that the Install-Fests have helped dispel some
   misinformation regarding Linux ("people know about linux, but have
   the wrong info").
 - Alaa notes the need for more funding to continue all these efforts as
   well as the need for a "respectable" entity (ie. something registered) in
   order to convince academic institutions to listen to what is being said.
 =>Arabeyes to look on how best to help egLUG penetrate educational/academic
   institutions to spread the Open Source and linux ideas
 - Some questions regarding cooperating with other Arab LUGs were brought up,
   to which Alaa noted lack of realism.  Alaa stated that LUGs are very local
   in reach and requires some physical contact most of the time.  The need to
   spruce up http://www.OpenArabia.org was brought up to share in various
   LUGs' experiences, etc.
 - Alaa admits that documenting success stories (from any LUG) should be
   mandatory since it brings so much validity to those learning about OSS
   and linux.
 - Alaa noted the need for more developers, the need for a fully arabized
   Mandrake, the need for a VERY stable and easy to use Arabbix as well as
   documentation.
 - Alaa concludes by noting, "the tech savvy are moving to GNU/Linux on their
   own (or are too invested in proprietary) and the small enterprise and
   helpless individuals are too newbie'ish so they need all the help
   we can give them".
 - Mostafa anticipates at least 2 more install-fests in the upcoming year.
 - Uniball notes the creation of a new LiveCD called phaeronix based on LFS.

-- Topic: Trolltech/QT
-- By   : Lars Knoll
 - Lars starts with a short introduction by noting his responsibilities
   which include everything related to i18n in the Qt Library.
 - Lars also notes that in KDE he's one of the maintainers of the KHTML
   library (the one Konqueror and Safari uses).
 - cuco asks when RLM/LRM input support will be added, to which Lars notes
   Qt 4.0 release by the latest
 - Youcef notes that he has a page full of Qt bugs to be looked into and
   commented upon (http://wiki.arabeyes.org/QtBugs)
 =>Lars promises to look into the QtBugs URL noted prior to Qt-4.0
 - Lars notes that Qt-4 is slated for early 2005
 - Lars, answering a question, notes that Qt uses an internal Bidi
   engine which he wrote himself.
 - Lars notes that if there are any Qt specific bugs (ie. they appear
   in Qt only apps) to send a report to qt-bugs (at) trolltech.com
 - Munzir, among others, complains about the Qt bug tracking not being
   transparent thus not knowing what is a known or unknown bug (this leads
   to lots of repetitive work from various people to find the same bugs
   since a list of known bugs is publicly not available).  Lars notes that
   not being transparent is to be expected since Trolltech is a company
   which needs to respect its commercial customers' privacy when _they_
   send in bugs.
 - Lars suggests CC'ing one of Arabeyes' mailing-list when submitting bugs
   that way the mailing-list archives will serve as a public trace of the bug.
 - Shachar asks about the procedures if someone else wants to solve bugs in
   QT to which Lars notes the need for copyright assignment for larger patches
   else small patches are welcome.
 - Munzir asks about a bug in Kword which is non-existent in Kmail and ponders
   why the difference.  Lars responds with Kword is doing its own text layout
   (so its a KDE bug and not a QT one).
 - cuco inquires about an option to set the location of the scroll bars on
   a QScrollView or use the reverse layout system per widget instead of per
   "desktop" this will be handy in khtml for example.  Lars notes that they
   are thinking about this for the 4.0 release.
 - Lars notes that he's often on IRC (#kdelounge) in case he's needed.

-- Topic: Arabeyes - Duali Project
-- By   : Mohammed Elzubeir (aka kuru)
 - Mohammed starts by noting that Duali is the open source Arabic spell
   checker (http://www.arabeyes.org/project.php?proj=duali)
 - Mohammed notes that its development has been slower than had hoped
   but he's working to remedy that issue soon
 - Mohammed notes the slow development cycle is due to the fact that a proper
   data set is not available to implement Duali as initially envisioned.  The
   needed compact dictionary for is not in-hand.  For more info on the details
   look into http://www.arabeyes.org/~elzubeir/duali/duali.html
 =>Arabeyes to look into what is involved to create the aforementioned
   spelling dictionary.
 - Mohammed notes that the current temporary solution is prefix/stem/suffix
   based.
 - Mohammed notes that Duali's license is based on BSD while the spelling
   dictionary (if/when it appears) will be GPL'ed.
 - Shachar asks if hspell helped at all in Duali's development to which
   Mohammed notes that he's exchanged a few email with its author but hopes
   to have a comprehensive solution in the future.
 - Mohammed notes that Duali will integrate easily with all applications
   (like KDE, OpenOffice.org, etc).
 - Shachar notes that a spell checker needs to be fast ("something learned
   from hspell") and that talking to nadav or tzafrir would be wise.
 - Mohammed notes that the spelling dictionary would contain all valid Arabic
   root words as well as static strings (names, foreign words, etc).
 - Nadim ponders if by adding the proper prefix and suffix would result in
   a list of all the valid Arabic words.  Mohammed notes that it would.
 =>Arabeyes to look into the spelling dictionary as a means to generate an
   Arabic wordlist for its future Arabic->English translation project.
 - Uniball notes the need for an Arabic grammar checker as well which could
   tie-in well to duali's functionality (noting that he'll look into this).
 - Nadim asks if aspell is an integration option.  Mohammed responds with
   "unlikely" but its APIA/interface could be identical.
 - Mohammed notes that the response (and thus possible needs) to Duali has
   been rather negligible.
 - Mohammed notes that the spelling dictionary will require lots of time to
   create to which Youcef notes doing progressive checks (enrich by practice)
   to encourage development and contribution (to see progress).
 - Pablo notes that current alpha versions of aspell uses affix compression
   and that hspell is being planned on getting integrated into aspell
 =>Mohammed to look into aspell's recent changes to possibly re-spark duali
   integration talks and/or thoughts.

-- Topic: Arabeyes - Katoob Project
-- By   : Mohammed Sameer (aka Uniball)
 - Mohammed notes that Katoob is a unicode/bidi aware text editor initially
   designed to cater to the Arab user's needs which has grown to support more
   languages of late.
 - Mohammed notes that Katoob was developed prior to Gtk's Bidi support, but
   since GTK2 (thanks to the GTK+ team) now supports Bidi its functions will
   be used in lieu of those written specifically for Katoob.
 - Mohammed notes that he tries to tie Katoob to the various projects
   Arabeyes hosts/develops and so Katoob is able to query Arabeyes Dictd
   dictionary, will add the proper hooks for Duali, etc.
 - Shachar asks about letting the user override the paragraph direction to
   which Mohammed notes that the user can insert unicode control chars to
   get that affect.
 - Various people ask about when to expect the next release to which Mohammed
   notes "maybe this month".
 - Various people praise Katoob and note that its a great application that
   many have come to depend on.
 =>Mohammed to include a .deb with next release
 - When asked how does Katoob differ from others (like Gedit) Mohammed notes
   that Katoob is better suited for Arabic users.  He then gave the keyboard
   emulator as an example (it'll allow you to type Arabic even if you don't
   have an Arabic keyboard configured).
 - Mohammed also stressed Katoob's low memory usage compared to others.

-- Topic: Gnome
-- By   : Carlos Perello Marin
 - Carlos starts out by noting that he's working on GNOME doing several
   l10n/i18n tasks like translations and programs.
 - Carlos notes that his main project is the Translation Status pages
   which denotes all of Gnome's translation status info.
 - Carlos notes that within GTP (The GNOME Translation Project) they have
   one main mailing-list (gnome-i18n (at) gnome.org) and sub-teams for each
   and every locale.  Carlos also notes that they are open about how the
   teams operate (ie. no mandatory policy from Gnome's side) yet they expect
   each team to have a team-leader (or coordinator) to interface with and
   to give CVS access to.
 - Carlos notes that Christian Rose is GTP's coordinator.
 - Carlos notes http://l10n-status.gnome.org to see current translation status
 - Various people note that Arabeyes has its own internal status on its CVS
   pages, for Gnome its http://www.arabeyes.org/misc/gnome_status_bar.html
 - Various people (olimar, kuru, etc) note some long existing problems as well
   as lack of Right-to-Left (RTL) support in various popular applications
   (such as evolution).  Carlos replies with the need to know what to fix.
 - olimar notes that he had a disturbing conversation with evolution's
   developers in which they noted "we don't use RTL, so don't expect it
   anytime soon" which got replied with Arabeyes needs to step-in and help.
 =>Arabeyes to enumerate the applications within Gnome that lack RTL/Arabic
   support and commence patch generation
 - kuru wonders if Pablo's suggestion about an 'Arabic GUI requirements' doc
   would help.  Carlos replies with a yes.  kuru coins its the "BIG" doc.
 =>Arabeyes to forward the developer's Arabic requirements Guide to Gnome once
   completed desktop-devel-list (at) gnome.org and gnome-i18n (at) gnome.org
 - Shachar notes that there is a company working on BiDi for evolution-2.0

-- Topic: Arabeyes - Bayani Project
-- By   : Youcef Rabah Rahal (aka yuyu)
 - Youcef starts out with a very detailed outline of his presentation :-)
 - Youcef notes that Bayani is a mathematical and scientific toolkit to
   draw graphs, perform symbolic computations, fit data, etc
 - Youcef reminders all that his main intended audience for the package
   was/is high-schoolers, university students and researchers.
 - Youcef notes that there are solutions out there, but they usually are
   aimed at specialists and they're usually not comprehensive solutions
   (some plot graphs, others that do symbolic computation).
 - Youcef notes that Bayani is very likely to be the first open source,
   FREE and COMPLETE solution of its kind.
 - Youcef then outlines the project's history as well as its implemented
   and planned features.
 - Youcef notes that Bayani is localized to Arabic, English and French and is
   able to accept those three languages in its symbolic computation language.
 - Various people are confused on how the Arabic aspects kick-in within Bayani
   to which Youcef replies with examples citing that anyone can enter
   equations in symbolic Arabic without issue along with graph text, etc.
 - Youcef, answering a question, notes that Bayani uses the QT toolkit and
   is fully under the GPL license.
 =>Anyone to help Youcef with autotools and packaging the application
 - Shachar suggests learning from "netchat" in sf.net (for autotools)

-- Topic: Wine
-- By   : Shachar Shemesh
 - Shachar starts out by defining what Wine stands for "Wine Is Not
   an Emulator" adding that Wine is an attempt at reimplementing the
   entire Win32, Win16 API all written in C, 100% from scratch.
 - Shachar notes that most of the development is done by a company called
   "CodeWeavers", who built a proprietary product around Wine, called
   "CrossOver office" (aimed at Office, IE, photoshop, etc).
 - Shachar's area of interest within Wine is Bidi and issues related to it
 - Shacher notes that the Bidi code is mostly based on ICU.
 - Shacher would have liked to use fribidi, but its lack of UTF-16
   (which is what Windows uses) support is a major issue.
 - Shacher notes that the Bidi support within Wine is at approx. 80% done
 - Shachar notes that the work on BiDi is rather immature making it rather
   easy to find bugs in Wine BiDi
 - Shachar notes http://www.winehq.org with special emphasis on "franks
   corner" for those looking for the basics
 - When asked how hard it would be help out and to contribute quickly,
   Shachar replies noting that it all depends on one's Win32 coding skills.
 - Shachar notes that it should not take more than a month for anyone to
   really be productive and start offering some much needed patches.
 - Shachar details a bit the process of how things are compiled
 - Shachar notes that most of the development happens via trial-and-error
   as well as debug outputs from the Win32 API
 - Replying to a question, Shachar details the differences between VMware,
   Win4Lin and Wine
 - Shachar replies to "do you guys peek at any proprietary code to get ideas
   on how things are done" by noting that the Wine team is _extremely_
   sensitive about this and takes active measures to distance themselves
   from such things (in short, NO).
 - Shachar adds that they do read/study what is noted on microsoft's own
   site http://msdn.microsoft.com
 =>Arabeyes to assist Shachar in adding Arabic shaping
 - Shachar notes that the biggest area in need of further work is the "edit
   controls" (ie. the handling of the various Escape/Control characters
   to do such things are moving the cursor, skipping words, etc)
 - Nadim notes that there is no set/proper standard on how things need
   to be done with regard to Bidi and "edit controls" since a couple of
   projects (mlterm and PuTTY) have/will deal with this exact subject.
 - Shachar notes that some prelim work had taken place yet its rather
   incomplete and requires quite a bit of review and potential rewrite,
   http://imagic.weizmann.ac.il/~dov/Hebrew/logicUI24.htm
 =>Nadim via Arabeyes is to engage with Shachar regarding the "edit
   controls" doc to bring forth a potential specification
 - Shachar notes that he's often in #winehackers

-- Topic: Lebanon LUG
-- By   : Jad Saklawi, Hisham Mardam Bey
 - Jad notes their website, http://linux.com.lb
 - Jad notes the LUG has lots of face-to-face meetings (2 per month)
 - Hisham goes through a history and background of the LUG noting that the
   LUG is now approx 140 people
 - Jad notes that they're now concentrating on Public Relations (PR) and media
 - Hisham notes that they've created Lebanon's first distro called "Embark"
 - Jad notes that a new application called "path" was developed which he
   believes will benefit others, http://wolf.funinc.org/b2evo/blogs/linux.php
 - Jad notes that the LUG also conducts various lectures at many universities
   along with tutorials for newbies and LUG members
 - Jad notes that the LUG is "less Arabic oriented"
 - Nadim asks whether thought was given to combine efforts on the various
   distro work taking place, Jad replies noting that no one was interested
   in helping other distros (like Arabbix)
 - All agree that the distro's content are less important compared to the
   accompanying documents and HOWTOs on how to develop one's own (ie. the
   knowledge base is what matters and not necessarily the end result).
 - Hisham explains a bit more what the LUG's new "path" application is about
 - Jad fielding a question about working with other LUGs by noting that he's
   contacted most of the Arabic LUGs in the past at which point OpenArabia.org
   is mentioned with a plea to help it out

-- Topic: OpenOffice.org
-- By   : Pavel Janik, Eike Rathke
 - Pavel starts by noting that he leads the Czech Native-Lang project, co-lead
   of l10n project and member of OpenOffice.org (OOo) Community Council
 - Pavel notes his pleasure in seeing the inclusion of Arabic into OOo and
   hopes for further mutual help and assistance to get things fully right
 - Eike then notes that he's co-lead of Calc, the spreadsheet application,
   and also responsible for coordinating the application relevant work in
   the i18n framework
 - Eike notes that now he's mainly adding the locale data
 - Munzir announces that he and his colleagues have fully translated OOo
   to Arabic 
 - Pavel notes needing more help in debugging Arabic and offers an example,
   http://www.openoffice.org/issues/show_bug.cgi?id=32179
 - When prompted about Arabic-Indic numbers, Eike notes that they can easily
   be integrated into Calc for instance and one should be able to use both
   those numbers and their latin counterparts.
 =>Munzir to verify Arabic-Indic number support
 - Munzir notes that he's listed all the OOo bugs on Arabeyes' Wiki page,
   http://wiki.arabeyes.org/OpenBugs
 - Pavel, upon probing, notes that using .PO files for translations was/is
   being done for the Czech team and is likely to spread.  Pavel notes the
   existence of many GSI-to-PO conversion scripts and offers a URL of one.
 - Munzir notes some screenshots for others to inspect upcoming support,
   http://tmp.janik.cz/OpenOffice.org/Arabic/Screenshots
 =>Munzir to upload relevant OOo screenshots to http://art.arabeyes.org
 =>Munzir to transcribe OOo bugs noted in http://wiki.arabeyes.org/OpenBugs
   to OpenOffice.org's IssueZilla tracking system (cross-referencing)
 - amaoui notes the need to fix a nasty bug relating to 'harakat' and shaping
   as can be seen here, http://amaoui.free.fr/misc/images/oo-ar.png
 =>Amaoui to submit a bug report to OOo as well as Arabeyes' Wiki Bugs page
 - Pavel notes that he's reachable at pjanik@oo.o (do the expansion :-)

-- Topic: Pango
-- By   : Owen Taylor
 - Owen notes that he's designed and maintains Pango, he's also been
   involved in GTK+ for a very long time in various roles.
 - Owen then proceeds to outline the missing and known bugs in both
   Pango and GTK+
 - Owen notes wanting/needing a proper document/specification for "editing
   changes" instead of relying on some ad-hoc methods
 - Owen notes that all volunteers are welcome in assisting him
 - Nadim notes that Shachar has just asked for a similar doc and probes Owen
   on his interest to get involved as well.  Owen notes that he's not the
   right person for the creation of such a spec and that Behdad Esfahbod and
   Dov Grobgeld would be as they've noted in the past their interest.
 =>Nadim to ping Behdad and Dov for their input on "edit/escape controls"
 - Various people ask about misc bugs and how best to see if the bug is
   in the underlying library or the application, Owen notes the need to
   use 'testtext' (its a test program built with GTK+) or 'gtk-demo'
 - Owen mentions that 'cairo' (cairographics.org) might be on interest to
   people as well as this doc http://people.redhat.com/otaylor/guadec5
 - Owen notes that he's recently made gnome-print work better with
   Pango - so programs like gedit now print Arabic correctly
 - Owen notes that he can usually be found on irc.gnome.org:#gtk+

-- Topic: Arabeyes - Gnome Translation Project
-- By   : Arafat Medini (aka olimar)
 - Arafat starts by noting that he's the Gnome Translation Coordinator
   for Arabeyes.org
 - Arafat notes that he wants to represent all translator's that have
   contributed and continue to do so via Arabeyes
 - Arafat outlines in brief who and how things are done and notes the
   existence of an ecosystem in which ideas can be exchanged
 - Arafat notes that the key focus now is Quality Control which has resulted
   in the creation of an Arabeyes wide Quality Assurance Committee (QAC) which
   is tasked with unifying and standardizing the translation process/terms
 - Arafat fielding a question notes that he expects in 1-1.5 years to have
   all of Gnome's files translated not just core
 - Arafat notes his favorite pet-link http://planet.arabeyes.org :-)
 - Arafat notes that he prefers to use Kbabel since gtranslator has bad
   formatting bugs which destroy one's file when changing its encoding
 - Arafat notes that for the 2.6 release he worked 22 hours a day on it
 - Arafat notes that all translation/documentation discussions take place on
   Arabeyes 'doc' mailing-list (doc (at) arabeyes.org).  It and other lists
   are noted here, http://www.arabeyes.org/mailinglists.php

-- Topic: Iraq LUG
-- By   : Hakim George
 - Hakim starts out by noting the LUG was in the works for a few years but
   due to the difficulty of those in Iraq to communicate its been on hold.
 - Hakim notes that the LUG is made-up of about 10 active members of which
   3 are inside Iraq proper (the others are primarily in England)
 - Hakim states that the LUG's main goal to expand the use of linux in Iraq
 - Hakim believes that linux has a good chance in Iraq due to its low cost,
   lack of intellectual property headaches, etc
 - Hakim notes that those in Iraq have started contacting various educational
   institutions to introduce them to linux
 - A LUG member notes that Ashraf, in Iraq, contacted the head of computer
   engineering department of University of Baghdad and presented about Linux.
   The department head was very interested in Linux and its strengths.
 - Hakim notes that the LUG is considering the creation of a training program
   (Linux Computer Driving License of sorts) akin to various others on the
   web, http://www.icdl-unesco.org/Arabic%203.0%20Syllabus.pdf
 - Hakim notes that the main problem they're facing is the insecurity and
   instability in the country as a whole, needless to say linux is not a
   priority in people's minds
 - Hakim notes that Iraq fairs well in terms of availability of software and
   hardware so that's a non-issue
 - Hakim notes that the volunteers in Iraq are on daily contact with the LUG
   via email
 - A few people note that without focus on recruiting more member locally
   (ie. in Iraq) the LUG is falling short of its goals
 - Mahdi, an Iraq LUG member, notes the following URL for more info,
   http://www.iraqilinux.org/modules.php?name=News&file=article&sid=26
 - Hakim notes that he and others are available at #iraqilinux
 
-- Topic: Arabeyes - PuTTY/MiniBidi/CUPS Projects
-- By   : Ahmad Khalifa
 - Ahmad starts out by noting that he's from Egypt and that he's relatively
   new to Arabeyes/Linux/OpenSource (since January of '04)
 - Ahmad notes that he's developed mainly on windows and so PuTTY was
   suggested to him as something he might partake in
 - Ahmad adds that due to some perceived license issue with the bidi library
   of choice (PuTTY being MIT while Fribidi is GPL) it was decided to write
   an MIT-licensed light Bidi library - MiniBidi
 - Ahmad notes that MiniBidi will get a bit more attention to correct some
   minor issues and add the missing functionality
 - Ahmad also notes that CUPS (the printing daemon) is being worked on to
   include Bidi support
 - Ahmad notes that his Bidi code in CUPS is primarily to the text rendering
   (texttops) sections
 - It was noted that care should be taken with CUPS for those applications
   that do do their own Bidi
 - Ahmad, fielding a question, notes that a simple tutorial on Bidi/Shaping
   would have really helped him come to speed quicker.
 =>Ahmad via Arabeyes to look into writing a brief tutorial to help newbies
   with regard to Bidi/Shaping and related issues
 - Munzir mentions 'u2ps' (akin to a2ps)
 - Ahmad notes that he'll continue with CUPS upon which time he'll start
   looking into the PuTTY and "edit/escape control" stuff

-- Topic: Debian-boot
-- By   : Steve Langasek (aka vorlon)
 - Steve starts out by noting that he's rather new to all the intricacies of
   the Arabic language and that he simply added Bidi to debian's installer
   (d-i) due to various requests.
 - Steve notes that the Bidi support is within the slang library
 - Steve notes the code as it stands now does bidi and shaping but no
   ligatures/combining characters.
 - Steve goes into a bit of detail on how and where the code was implemented
   with regard to where would the code be a best fit noting that its currently
   being done with the application that is outputting the data and not the
   terminal 
 - Steve notes that in the next round, he'd like to add Bidi support to
   bterm proper (the Unicode-capable terminal that runs on the Linux
   framebuffer) and not the apps running on top of it
 - Steve notes he'd be more than happy if someone were to volunteer to
   starts looking at bterm and adding Bidi support to it
 =>Arabeyes to start looking what is required to add Bidi to bterm (d-i)
 - Anmar notes the need for Arabic support in all the APT tools such as,
   aptitude and dselect.  Steve notes that Arabic support will be needed
   in ncurses then.
 - Steve also mentioned jfbterm as the terminal post installations
   (ie. after bterm is used, jfbterm is opted for)
 - When asked about BiCon, Steve notes not being familiar with it
   http://www.arabeyes.org/project.php?proj=bicon
 - After a brief description on BiCon, Steve notes that it might be a useful
   application to use post installation
 - Steve notes that there's currently no one working on an overall design
   for Arabic support in Debian right now and that ought to change
 =>Arabeyes to look into a deeper engagement with Debian to better facilitate
   Debian's Arabic support and strategy
 - All seem to agree that bterm should really be replaced with something
   better since it doesn't deal with ligatures (ie. harakat)
 =>Steve & Arabeyes to look into BiCon as a possible replacement for bterm
 - Questions arise about what BiCon would do if a framebuffer were NOT present
 - A query is made asking why can't jfbterm be used instead of bterm, Steve
   isn't sure and guesses that it might be a size related issue
 - When asked if the patch to slang was accepted upstream (ie. within
   the slang project itself), Steve notes that he's unsure
 - Steve notes that he and his colleagues are available on #debian-boot

-- Topic: Arabeyes - Arabbix Project
-- By   : Anmar Oueja
 - Anmar starts by noting that Arabbix is the first Arabic LiveCD ever and
   that its morphix based (http://www.arabeyes.org/project.php?proj=Arabbix)
 - Anmar notes that Arabbix's sole purpose is to introduce all Arabic
   speakers to linux and to showcase Arabeyes's various projects and results
 - Anmar notes that Arabbix is Arabic from the get-go (everything is
   defaulted to Arabic) unlike many newer distros
 - Anmar notes that version 0.8 was released about a year ago and 0.9-alpha
   was just sampled in hopes of a full 0.9 later this year addressing 0.8's
   various deficiencies
 - Various questions about Arabbix's progress are raised some of which note
   it as the catalyst for the flurry of new similar distros, Anmar replies
   by saying that he's more than happy to work with whomever is interested
   on Arabbix
 - Anmar notes that the majority of the necessary docs are on the morphix
   site (http://www.morphix.org)
 - Comments are made regarding the need to ease and accept contributors to the
   project as a whole, most are interested in helping, its noted, yet none
   know what to do and how to proceed.  kuru notes his concern about the
   future of the project since its not being made easy enough for others to
   build upon instead of reinvent.
 - Anmar notes that to build Arabbix from scratch its rather simple and the
   steps have already been documented on Arabeyes' CVS
 - One area of problems has been morphix's HD-installer, Anmar thus has been
   considering Mepis' HD installer instead.
 - Anmar notes using the 'developer' mailing-list on Arabeyes to do most/all
   of his communication, http://www.arabeyes.org/mailinglists.php
 - Anmar notes all help is welcome/appreciated


Arabeyes.org is ecstatic at the turn-out and at the very strong content
and professionalism of all those involved.  Many thanks to all those that
presented and took time off their busy schedules and our warmest wishes
to all the attendees.  Arabeyes will attempt in good spirit to follow-up
with all the TODOs listed above to bring resolution and closure to the
various issues raised.  Arabeyes is the collective sum of all its volunteers
along with their hard work, vision and commitment; without them we not
only would not exist we've have no purpose to contemplate existence.

Salam.

 .:Arabeyes.org Team:.
