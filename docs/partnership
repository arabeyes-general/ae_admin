/************************************************************************
 * $Id$
 *
 * ------------
 * Description:
 * ------------
 *
 * This document lays down the ground rules for any kind of cooperation/
 * collaboration Arabeyes will have with any other group/organization. 
 *
 * -----------------
 * Revision Details:    (Updated by Revision Control System)
 * -----------------
 *  $Date$
 *  $Author$
 *  $Revision$
 *  $Source$
 *
 ************************************************************************/

-----------
Definitions
-----------

Here are some terms that are used throughout the document,
- Volunteer:	someone who voluntarily undertakes a task/work.
- 3rd-party:	used loosely to indicate a third party (company,
                organization, group) which is interested in working
	        with Arabeyes (in sharing resources, developers,
	        contacts, etc) to achieve the same ultimate goal
	        of spreading and preaching linux/unix to the Arab
	        world.
- contributors:	encompasses developers, translators and anyone
                who assists in any capacity.
- *ix:		used to mean linux/unix of all flavors.

-------------
A. The Basics
-------------

1. Arabeyes' NO compromise points:
   * Arabeyes will only contribute to Free/Open Software Projects in which
     source-code or any other relevant results are available to the general
     public at no charge and in which changes and modifications are allowed
     to be distributed and/or are echoed back the original author(s)'
   * Arabeyes will be driven by the users' needs and by volunteers' efforts
   * Arabeyes efforts will remain vendor neutral and distribution neutral
   * Arabeyes will maintain its independence/freedom to make its own choices
     and decisions (technical or otherwise).
     + Arabeyes will retain its right to engage in any form of
       partnerships/cooperatives without the need to get permission from
       any third party (including but not limited to current/prior partners)
   * Arabeyes will only engage in equal partnerships/cooperatives
     + "Arabeyes will NOT dictate action, NOR will it be dictated to"
     + "Arabeyes will maintain technical excellence at all times"

2. Arabeyes Offers:
   * An organizational hub for various arabisation development efforts
     + Infrastructure for new as well as old arabisation projects
     + Communication mediums for usage by/for projects & teams
     + Ability to recruit developers and translators
     + Bleeding-edge tools/apparatus ensuring world-wide team access
   * Advocation and education of the arabisation of *ix systems
   * Committed Talent (technical, organizational as well as motivational)
   * Successful track-record and history of accomplishments

3. Arabeyes Requires:
   * Help in reaching more developers and interested parties (recruiting)
   * Help in making sure that the Arabeyes projects reach those who need it
   * Assistance in ensuring Arabeyes' continual existence, by:
     + Covering the running cost of Arabeyes (currently volunteer-funded)
     + Providing a means to Upgrade connection/hardware/space when needed
     + Preserving the volunteers' interest in Arabeyes via the creation of an
       incentive-plan (reward volunteers who are contributing to Arabeyes) 
   * Assistance in propelling the recognition of 'Arabeyes', by:
     + Media coverage of Arabeyes' work (Tech features, News releases)
     + Involving Arabeyes in arabisation committees/decision-making circles
     + Having a presence in ALL *ix arabisation/development workshops/seminars

---------------------
B. Details - Manpower
---------------------

1. Which project next:

All projects, schedules and milestones relating to mutual work will be
discussed and agreed-upon a priori.  There might be instances where work
will commence prior to final agreement, but agreement and consensus MUST
be met before fully getting engulfed by technical details.

A representative or a contact person from each organization will act as
liaisons and "contact-point" to ensure superior communication.  A weekly
"status-report" will also be required from each end which will be made
available to the public noting "This week's accomplishments (did)" as
well as "Next week's accomplishments (todo)".  Each new project under-taken
will have its own project page (on Arabeyes or elsewhere) to educate as
well as ensure continued progress.

Any monetary involvement (or incentives for that matter) in any shape or
form will need to be declared publicly and need to be announced prior to
any agreement taking shape.  The monetary involvement could include but is
not limited to compensation to 3rd-party employees, donations or payment
to various involved entities or organizations or quid pro quo arrangements
among others.  In short, if money is involved on either side in any level
of the execution or management hierarchy, it needs to be declared.

2. How will the work proceed:

All details, technical and otherwise, will be subject to the project or
sub-project maintainer's approval as regulated by Arabeyes rules and
policies.  The various details are thus outside the scope of this document
and are likely to differ from one project to the next.

3. Where will the work reside:

All files/code/translations/documents will reside on Arabeyes' CVS
repository.  That is important because it ensures the centralization
of the efforts, making future parties more inclined to follow in-line.
If Arabeyes is to mirror then 3rd-party should have CVS access for
involved Arabeyes members.

4. Education/Backgrounding:

It is expected that all 3rd-party contributors get informed of the
virtues of Open Source and volunteerism (Arabeyes can assist in this
effort if need be).  It is also expected that the 3rd-party will
encourage its contributors to further engage with Arabeyes via
outlining the benefits to the overall community at large upon
completion of agreed-upon tasks.  The intent by all parties involved
should be to contiguously grow this community and to foster a sense
of constant renewal.

5. Communication:

3rd-party contributors are expected to use Arabeyes' various mailing
lists, to reflect and garnish support for any upcoming activities on
a pre-determined specific sub-project.

3rd-party contributors are also expected to commit work in most
instances (baring any technical extremes) on a regular basis.  The
contributors are expected to attain a sufficient level of tool usage
to attain this goal.

6. Credit:

Regarding any work that Arabeyes will do - credit for that work will
first and foremost go to Arabeyes (as a project) and then the various
individuals or entities per said agreement.  Reason being - the Arabeyes
project and its continued existence is far more important than any one
person or effort and as such should be held in higher regard.

----------------------
C. Details - Donations
----------------------

1. Detached Donations

A detached donation is one in which funds is donated to Arabeyes'
general fund without any strings attached.  In other words, the
funds are given to Arabeyes to do with it as it sees fit given the
donation expenditure guidelines within Arabeyes are met.  All
amounts, dates, donors (unless requested otherwise) will be publicly
noted and available.

2. Attached Donations

An attached donation is that in which funds are only donated given
these funds are spent on a particular project/task (not individual).
In these instances Arabeyes requests that a minimum of 3 options
(projects/tasks) are presented for Arabeyes' managers to select from.
If agreement is reached and a schedule is adopted 50% of the donation
will be requested prior to start of work with the remaining 50% to be
delivered upon completion.  All finances are to go to Arabeyes' general
fund.  Arabeyes is then to dispose of any amounts deemed necessary per
its expenditure guidelines.  Any disputes and/or discussions with regard
to payment or project delivery are to be made available to the public
in a timely manner.  All amounts, dates, donors (unless requested
otherwise) will be publicly noted and available.

3. Attached Donations to Individual(s)

An attached donation to individuals is that in which funds are only
donated given certain individuals are to benefit from said funds.
Arabeyes will not take any part of such transactions and is thus
not liable nor responsible for them.  Donations, as far as Arabeyes
are concerned are for the benefit of the project and community at
large and not select individuals.

