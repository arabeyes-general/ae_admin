#--
# $Id$
#
# This is a place holder document - it will be folded into another and
# is only kept here as a half-way point (ie. it will be deleted).  This
# doc is to hold the charter translation of the current existing doc
# along with some changes as they relate to the 'core' team and its
# make-up.  Once the new charter is completed it will be entirely translated
# to Arabic.
#
#--

                             Charter.Translation


1. Arabeyes is a Meta/Umbrella project that is aimed at fully supporting the
   Arabic language in the Unix/Linux environments.  It is designed to be a
   central location to standardize the Arabization process under the Open
   Source license/mantra (in development, translation, documentation, etc).
   Arabeyes relies on voluntary contributions by computer professionals and
   enthusiasts all over the world.  Arabeyes is subject to all points noted
   within the document to sustain its existence as well as thrive and grow.

2. This Charter/Handbook is Arabeyes' internal working directive and rule
   book regarding all its development and translation work.  This Charter is
   effective of February 24, 2003 as was agreed unanimously by Arabeyes'
   management team.

3. Term definition or nomenclature
   - Team: the Arabeyes active volunteers 
   - Core (or core-team): Arabeyes' internal meta-project management team

4. Goals and Aspirations
   a. Vision/Aims
      1. Bring forth Arabic support to all aspects of Linux/Unix to satisfy
         all end-user's needs.
      2. Become a central repository/location for all issues with regards to
         Arabization and Linux/Unix/Open-Source.
      3. To raise people's awareness about Arabeyes' work, its needs, its aims
         and all underlying Arabic-centric issues.
      4. To recruit and find capable individuals so as to bring forth the
         required work and attain its goals while increasing people's
         abilities and raising their potential.
      5. To find the proper support internally and externally to upkeep this
         project be it financially, technically or via any other means.
      6. To get involved in various consotrium organizations to better
         voice the Arabic viewpoint.

   b. Misson/Goals
      1. Increase the number of volunteers working on Arabeyes projects.
      2. Encourage and support new volunteers by engaging all their needs
         for them to grow and to attain results.
      3. Create an environment in which the work will continue to exist and
         even flourish without the said involvement of the original
         authors in hopes of ever keeping the various projects on-going and
         ever evolving.
      4. Preach the virtues of Volunteerism and the ideals of Open Source
         and Free Software.
      5. Increase the Arab industry's awareness and get them engaged in the
         important goals/efforts Arabeyes is undertaking.

5. Arabeyes' Make-up/Populous
   a. Volunteers are made of those who voluntarily undertake a task/work
      with no regard to pay or any quid-pro-quo action.

   b. Core-team (or 'core') is,
      1. Made up of individuals who have risen to the occasion and have
         proved their seriousness and commitment to this project and love
         for it.
      2. All 'core' members are equal with regard to responsibilities and
         position.

6. Core-team's duties and responsibilities
   a. 'Core' takes care of the managerial as well as financial aspects of
      Arabeyes.
   b. 'Core' is in charge of outlining rules/laws with regard to its conduct
      given its charter is met and is applied.
   c. 'Core' is made up of an odd number at all time.  If for whatever reason
      a member is relieved or is inaccessible, his/her vote needs to be
      assigned apriori to an active member in writing.
   d. An annual election is conducted at a preordained date to elect all 'core'
      members

   Meetings
   a. Open meetings are to be held on a weekly basis on IRC.
   b. A meeting is initiated given quorum is met which requires at least 75%
      of the original 'core' members to be present.
   c. Only for/yes/yay and against/no/nay votes are accepted in the meetings.
   d. The largest shear raw number of votes (yay/nay) wins.

   Add/Subtract
   a. A 'core' member loses his privileges if,
      1. He/She resigns explicitly (done in writing and confirmed by all other
         'core' members)
      2. If all the 'core' members vote a certain individual out at which
         point either a special election is called for or the teams selects
         a respected replacement.

7. Duties/TODOs
   a. The gloabl Arabeyes list of Project TODOs is inspected on a monthly
      basis by the 'core' team which encompasses Arabeyes' overall goals
      and most urgent attainables.  This list is posted for all to view
      and inspect and to choose from.  This list is what gets advertised
      to newbies for them to select something to work on.
   b. The 'core' team as a whole has its set of duties as well (from
      maintaining the hardware, software to monitoring the lists and various
      databases).  The list of all of these maintenance issues will be
      compiled and is to be addressed by the team members as a whole.

8. Sub-Project Maintainers
   a. A maintainer is found/elected anytime the job calls for it (due to
      vacancy or someone leaving a position).  The selection is made by
      the 'core' members but is not exclusive to them.
   b. Anyone can be a Project Maintainer including 'core' members given
      they attain and retain an active CVS account.
   c. A project maintainers is to,
      1. Study said project, know enough about the external deadlines and
         demands to come-up with an internal schedule to present to his team.
      2. Follow his/her outlined schedule making adjustments when needed.
      3. Take full control of the project and is to track results, problems
         and be the bona-fide leader/champion of the project.
         The maintainer is to also recruit and incentivise his own group as
         well as maintain and create the relevant docs/instructions.
      4. Keep his team members and Arabeyes volunteers privy of the status
         of his/her project by sending a "status" report at least once a month
         to the appropriate mailing-list.
   d. The 'core' team has the right to relieve a maintainer given he/she
      are not adhering to the set rules and/or are not proceeding in the
      best interest of the subproject or Arabeyes at large (slow to no
      action will not be tolerated).

9. Openness/Transparency
   a. All meeting agenda, minutes and decisions are to be available in a
      public location for all to view.
   b. Certain sensitive issues/topics (as deemed by all of 'core' members)
      may be omitted given a mention is supplied in lieu.
   c. All financial issues are to be noted in a public setting for all
      (or at a minimum all financial contributor/donors) to access or
      inspect.  The finances include anything of monetary value coming
      into Arabeyes' general fund or leaving it.

